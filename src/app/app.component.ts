import { Component, ElementRef, ViewChild } from '@angular/core';
import Handsontable from 'handsontable';
import 'handsontable/dist/handsontable.full.css';
import * as jexcel from "jexcel";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild("spreadsheet") spreadsheet: ElementRef;
   data = [
    [1,2, -3],
    [4, 5, 6],
    [4, 5, 6],
    [4, 5, 6],
  ];
  ngAfterViewInit() {
    jexcel(this.spreadsheet.nativeElement, {
      data: this.data,
      columns: [
        { type: "text", width: "100px", title:"Session",color:"red",readOnly:true},
        { type: "text", width: "100px", render: "square",title:"Period" }
      ],
      updateTable(instance, cell, col, row, val, label, cellName) {
        if (cell.innerHTML == 'Session') {
            cell.parentNode.style.backgroundColor = '#fffaa3';
        }
        if (col == 2) {
          if (val < 0 ) {
              cell.style.color = 'red';
          }  if(val > 0) {
              cell.style.color = 'green';
          }
      }
      },
      mergeCells:{
        A1:[1,2]
    },
      minDimensions: [10, 10]
    });
  }

  colorchange(){
    'background-color:red';
    // if(this.data > 0){

  }

  // dataset = [
  //   {id: 1, name: 'Ted Right', address: 'Wall Street'},
  //   {id: 2, name: 'Frank Honest', address: 'Pennsylvania Avenue'},
  //   {id: 3, name: 'Joan Well', address: 'Broadway'},
  //   {id: 4, name: 'Gail Polite', address: 'Bourbon Street'},
  //   {id: 5, name: 'Michael Fair', address: 'Lombard Street'},
  //   {id: 6, name: 'Mia Fair', address: 'Rodeo Drive'},
  //   {id: 7, name: 'Cora Fair', address: 'Sunset Boulevard'},
  //   {id: 8, name: 'Jack Right', address: 'Michigan Avenue'},
  // ];


  // tableSettings: any = {
  //   //rowHeaders: true,
  //   //colHeaders: true,
  //   viewportColumnRenderingOffset: 27,
  //   viewportRowRenderingOffset: "auto",
  //   //colWidths: 150,
  //   height: 450,
  //   // allowInsertColumn: false,
  //   // allowInsertRow: false,
  //   // allowRemoveColumn: false,
  //   // allowRemoveRow: false,
  //   // autoWrapRow: false,
  //   // autoWrapCol: false,
  //  // stretchH: "all",
  //   width: 924,
  //   // autoWrapRow: true,
  //   //height: 487,
  //   maxRows: 22,
  //   manualRowResize: true,
  //   manualColumnResize: true,
  //   // rowHeaders: true,
  //    columns: [
  //       {
  //         data: 'id',
  //         type: 'numeric',
  //         width: 40
  //       },
  //       {
  //         data: 'currency',
  //         type: 'text'
  //       },
  //       {
  //         data: 'level',
  //         type: 'numeric',
  //         numericFormat: {
  //           pattern: '0.0000'
  //         }
  //       },
  //       {
  //         data: 'units',
  //         type: 'text'
  //       },
  //       {
  //         data: 'asOf',
  //         type: 'date',
  //         dateFormat: 'MM/DD/YYYY'
  //       },
  //       {
  //         data: 'onedChng',
  //         type: 'numeric',
  //         numericFormat: {
  //           pattern: '0.00%'
  //         }
  //       }
  //     ],
  //   colHeaders: ["ID", "Currency", "Level", "Units", "Date", "Change"],
  //   manualRowMove: true,
  //   manualColumnMove: true,
  //   contextMenu: true,
  //   filters: true,
  //   dropdownMenu: true,

  //   afterValidate (isValid,td, value, row, prop){
  //     if(value == false){
  //       	console.log( value, row, prop)    
  //         alert("Invalid")
  //         //Value = isValid
  //         // row = inserted invalid value
  //         //prop = row index changed
  //     }
  //     if (parseInt(value, 10) < 0) {
  //       // add class "negative"
  //       td.className = 'make-me-red';
  //     }
			
  //   }
  // };
  // dataset = [
  //   {
  //     id: 1,
  //     flag: "2",
  //     currencyCode: "3",
  //     currency: "4",
  //     level: 5,
  //     units: "6",
  //     asOf: "7",
  //     onedChng: 8
  //   },
  //   {
  //     id: 9,
  //     flag: "10",
  //     currencyCode: "11",
  //     currency: "12",
  //     level: 13,
  //     units: "14",
  //     asOf: "15",
  //     onedChng: 16
  //   }
  // ];

  // title = 'table';
  // tabelHeading:any[]=[]
  // trade = [
  //   { "id": 1, "session": "1", "Delivery_Period": "00:00 - 00:15", "headOne": "", "head2": "", "head3": "", "head4": "", },
  //   { "id": 2, "session": "1", "Delivery_Period": "00:15 - 00:30", "headOne": "", "head2": "", "head3": "", "head4": "", },
  //   { "id": 3, "session": "2", "Delivery_Period": "00:00 - 00:15", "headOne": "", "head2": "", "head3": "", "head4": "", },
  //   { "id": 4, "session": "2", "Delivery_Period": "00:15 - 00:30", "headOne": "", "head2": "", "head3": "", "head4": "", },
  //   { "id": 5, "session": "3", "Delivery_Period": "00:00 - 00:15", "headOne": "", "head2": "", "head3": "", "head4": "", },
  //   { "id": 6, "session": "3", "Delivery_Period": "00:15 - 00:30", "headOne": "", "head2": "", "head3": "", "head4": "", }

  // ]
  // rowGroupMetadata: any;
  // cols: any[];

  constructor() {
    /**
     * For Dynamic Header
     */
    // delete this.trade[0]["id"];
    // this.tabelHeading.push(Object.keys(this.trade[0])); 
  }
  ngOnInit() {
//     const data = [
//       ['', 'Tesla', 'Volvo', 'Toyota', 'Ford'],
//       ['2019', 10, 11, 12, 13],
//       ['2020', 20, 11, 14, 13],
//       ['2021', 30, 15, 12, 13]
//     ];

//     const container = document.getElementById('example');
// const hot = new Handsontable(container, {
//   data: data,
//   rowHeaders: true,
//   colHeaders: true
// });
    // this.updateRowGroupMetaData();
  }
  
  // updateRowGroupMetaData() {
  //   this.rowGroupMetadata = {};
  //   if (this.trade) {
  //     for (let i = 0; i < this.trade.length; i++) {
  //       let rowData = this.trade[i];
  //       let representativeName = rowData.session;
  //       if (i == 0) {
  //         this.rowGroupMetadata[representativeName] = { index: 0, size: 1 };
  //       }
  //       else {
  //         let previousRowData = this.trade[i - 1];
  //         let previousRowGroup = previousRowData.session;
  //         if (representativeName === previousRowGroup)
  //           this.rowGroupMetadata[representativeName].size++;
  //         else
  //           this.rowGroupMetadata[representativeName] = { index: i, size: 1 };
  //       }
  //     }
  //   }
  // }
  // show(){
  //   console.log(this.trade)
  // }

  detectChanges = (hotInstance, changes, source) => {
    console.log(changes);
  };
  
 
}
