import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { AppComponent } from './app.component';
import {TableModule} from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { HotTableModule } from '@handsontable/angular';

@NgModule({
  declarations: [
    AppComponent,
    
  ],
  imports: [
    BrowserModule,
    // CdkVirtualScrollViewport,
    TableModule,
    HotTableModule,
    InputTextModule,
    CalendarModule,
		DropdownModule,
		ButtonModule,

    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
